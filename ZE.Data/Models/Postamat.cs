﻿using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(Postamat))]
    public class Postamat : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PostamatId { get; set; }

        [StringLength(100)]
        public string AddressLine1 { get; set; }
        [StringLength(100)]
        public string AddressLine2 { get; set; }
        
        public string HowToFindDescription { get; set; }
        
        public ICollection<HowToFindPicture> HowToFindPictures { get; set; }
        
        [ForeignKey(nameof(City))]
        public Guid? CityId { get; set; }

        public City City { get; set; }        

        public bool WithScreen { get; set; }        

        public PostamatState PostamatState { get; set; }
        public PostamatStatus PostamatStatus { get; set; }
        
        [StringLength(10)]
        public string UniqueCode { get; set; }

        [InverseProperty("Postamat")]
        public ICollection<Locker> Lockers { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? MapZoom { get; set; }

        public string BarcodeString { get; set; }

        public byte[] Barcode { get; set; }

        [ForeignKey(nameof(PostamatForm))]
        public Guid PostamatFormId { get; set; }
        [InverseProperty("Postamats")]
        public PostamatForm PostamatForm { get; set; }

        public User CreatedBy { get; set; }
        [ForeignKey(nameof(CreatedBy))]
        public Guid CreatedById { get; set; }
        public DateTimeOffset Created { get; set; }
        public User UpdatedBy { get; set; }
        [ForeignKey(nameof(UpdatedBy))]
        public Guid UpdatedById { get; set; }
        public DateTimeOffset Updated { get; set; }
    }
}
