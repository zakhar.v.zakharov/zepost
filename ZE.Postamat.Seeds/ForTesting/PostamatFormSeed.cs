﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;

namespace ZE.Postamat.Seeds.ForTesting
{
    [SeedOrder(4)]
    public class PostamatFormSeed : BaseSeed
    {
        public PostamatFormSeed(DataContext context) : base(context) { }
        public override async Task ProcessSeedAsync()
        {
            var postamatForm = new PostamatForm
            {
                CellsCount = 10,
                Name = "For Testing",
                PostamatType = Data.Models.Enums.PostamatType.Mini
            };
            postamatForm = await AddOrGetRecordUniqueBy(postamatForm, p => p.Name);

            var lockerTypes = Context.LockerTypes.ToDictionary(t => t.Code, t => t);

            for(int i = 0; i < 2; i++)
            {                
                for(int j = 0; j < 5; j++)
                {
                    var loc = new LockerLocation
                    {
                        PostamatForm = postamatForm,
                        LockerType = lockerTypes["S"],
                        Left = lockerTypes["S"].Width*i,
                        Top = lockerTypes["S"].Heigh*j                        
                    };
                    if(postamatForm.PostamatFormId != Guid.Empty)
                    {
                        loc.PostamatFormId = postamatForm.PostamatFormId;
                    }
                    await AddRecordUniqueBy(loc, l => new { l.PostamatFormId, l.Left, l.Top });                    
                }
            }

            await Context.SaveChangesAsync();

        }
    }
}
