﻿using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.Models
{
    public class ParcelListModel
    {
        public Guid ParcelId { get; set; }

        public List<ParcelStateView> States { get; set; }

        public string Sender { get; set; }
        public string Receiver { get; set; }        
                
        public PostamatParcelViewModel Postamat { get; set; }

        public ParcelState ParcelState { get; set; }
        
    }
}
