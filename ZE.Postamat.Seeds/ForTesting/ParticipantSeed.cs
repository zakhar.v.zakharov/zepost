﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;

namespace ZE.Postamat.Seeds.ForTesting
{
    [SeedOrder(2)]
    public class ParticipantSeed : BaseSeed
    {
        public ParticipantSeed(DataContext context) : base(context) { }
        public override async Task ProcessSeedAsync()
        {            
            await AddIndividualParticipant(new Individual
            {
                CountryCode = "+7",
                Document = "1232 234323",
                DocumentType = Data.Models.Enums.DocumentType.Passport,
                Email = "zakhar.v.zakharov@gmail.com",
                FirstName = "Zakhar",
                LastName = "Zakharov",
                MiddleName = "V",
                PhoneNumber = "9852709403"
            });

            await AddIndividualParticipant(new Individual
            {
                CountryCode = "+7",
                Document = "8962 235423",
                DocumentType = Data.Models.Enums.DocumentType.Passport,
                Email = "egor.dementev79@gmail.com",
                FirstName = "Egor",
                LastName = "Dementev",                
                PhoneNumber = "9265896547"
            });

            await AddLegalEntityParticipant(new LegalEntity
            {
                Email = "testCo@mail.ru",
                FullName = "Test Incorporated Ltd.",
                ShortName = "Test Inc",
                IsActive = true,
                LegalAddressCity = Context.Cities.FirstOrDefault(c => c.Code == "MSK"),
                LegalAddressLine1 = "Tsvetnoy bulvar 2",
                PhoneNumber1 = "4951115555"
            });

            await AddLegalEntityParticipant(new LegalEntity
            {
                Email = "test2Co@mail.ru",
                FullName = "Test 2 Incorporated Ltd.",
                ShortName = "Test 2 Inc",
                IsActive = true,
                LegalAddressCity = Context.Cities.FirstOrDefault(c => c.Code == "MSK"),
                LegalAddressLine1 = "Podmoskovniy bulvar 6",
                PhoneNumber1 = "4951119999"
            });

            await Context.SaveChangesAsync();
        }

        private async Task AddLegalEntityParticipant(LegalEntity lEntity)
        {
            lEntity = await AddOrGetRecordUniqueBy(lEntity, i => i.Email);
            var p2 = new Participant
            {
                LegalEntity = lEntity,
                ParticipantType = Data.Models.Enums.ParticipantType.LegalEntity
            };
            if (lEntity.LegalEntityId != Guid.Empty)
                p2.LegalEntityId = lEntity.LegalEntityId;
            await AddRecordUniqueBy(p2, p => new { p.LegalEntityId });       
        }
        private async Task AddIndividualParticipant(Individual ind1)
        {
            ind1 = await AddOrGetRecordUniqueBy(ind1, i => new { i.FirstName, i.LastName });
            var p1 = new Participant
            {
                Individual = ind1,
                ParticipantType = Data.Models.Enums.ParticipantType.Individual
            };

            if (ind1.IndividualId != Guid.Empty)
                p1.IndividualId = ind1.IndividualId;
            await AddRecordUniqueBy(p1, p => new { p.IndividualId });            
        }
    }
}
