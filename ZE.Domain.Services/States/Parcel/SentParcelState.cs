﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;
using ZE.Data.Models.Enums;
using ZE.Domain.Abstractions.States;
using ZE.Domain.Abstractions.TechnicalServices;

namespace ZE.Domain.Services.States
{   
    public class SentParcelState : BaseParcelState
    {
      
        private readonly IBarcodeGenerationService _barcodeGenerationService;
        private readonly IEncryptionService _encryptionService;
        private readonly DataContext _context;
        private readonly ILogger<SentParcelState> _logger;
        public SentParcelState(DataContext context, IBarcodeGenerationService barcodeGenerationService, IEncryptionService encryptionService, ILogger<SentParcelState> logger) : base(context, logger)
        {            
            _barcodeGenerationService = barcodeGenerationService;
            _encryptionService = encryptionService;
        }
        public override ParcelState State => ParcelState.Sent;

        public override async Task PerformAsync(Parcel parcel)
        {
            await base.PerformAsync(parcel);
            if (parcel.DeliveryPaymentStatus == PaymentStatus.Paid)
                parcel.Paid = DateTimeOffset.Now;
            var info = await _encryptionService.EncryptAsync(parcel.ParcelId);
            parcel.BarcodeString = info;
            parcel.Barcode = _barcodeGenerationService.GenerateBarcode(info);
        }
    }
}
