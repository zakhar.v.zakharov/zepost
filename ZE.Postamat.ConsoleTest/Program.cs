﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Domain.Abstractions.Services;
using ZE.Domain.Services;

namespace ZE.Postamat.ConsoleTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
             var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
             optionsBuilder.UseNpgsql("Host=localhost;Database=ZE_postamats;Username=postgres;Password=Password12!", o =>                                                                                            
                                                                                              o.EnableRetryOnFailure());
             var dataContext = new DataContext(optionsBuilder.Options);
             await dataContext.Database.MigrateAsync();
             Console.WriteLine("Hello World!");
            return;
            using IHost host = CreateHostBuilder(args).Build();
            await DoAsync(host.Services);
            await host.RunAsync();
        }

        static async Task DoAsync(IServiceProvider services)
        {
            using IServiceScope serviceScope = services.CreateScope();
            IServiceProvider provider = serviceScope.ServiceProvider;

            var parcelService = provider.GetService<IParcelService>();

            await parcelService.CreateParcelAsync(new Domain.Abstractions.Models.ParcelComposeModel
            {
                Width = 20,
                Heigh = 15,
                Depth = 10,
                SenderId = new Guid("aae27a33-0bc7-4012-be4b-b2cb8d477c82"),
                ReceiverId = new Guid("32e2ee9f-e081-4800-919d-2138429bb096"),
                PostamatId = new Guid("4b1519a9-dfc3-4f4c-b4bf-a4a3e9c8bfde"),
                Description = "Tet Parcel",
                Sent = DateTimeOffset.Now,
                Weight = 1.53M
            });
            

            Console.WriteLine();
        }

        static IHostBuilder CreateHostBuilder(string[] args) 
        {
            return Host.CreateDefaultBuilder(args).ConfigureServices((_, services) => {
                services
                    .AddLogging(configure => configure.AddConsole())
                    .ConfigureDatabase("Host=localhost;Database=ZE_postamats;Username=postgres;Password=Password12!")
                    .ConfigureServices()
                    ;
            }) ;
        }

    }
}
