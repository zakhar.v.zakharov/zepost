﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum PostamatState
    {
        Available = 0,
        Unavailable = 1,
        UnderMaintenance = 100
    }
}
