﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZE.Data.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:delivery_state", "scheduled,pending,completed,cancelled")
                .Annotation("Npgsql:Enum:document_type", "passport,driving_license")
                .Annotation("Npgsql:Enum:locker_state", "closed,opened_for_delivery,opened_for_take_out,opened_for_maintenance")
                .Annotation("Npgsql:Enum:locker_status", "vacant,busy")
                .Annotation("Npgsql:Enum:parcel_cancellation_reason", "not_paid,sender_refuse,receiver_refuse")
                .Annotation("Npgsql:Enum:parcel_state", "created,sent,taken_to_delivery,pending_delivery,delivered_to_locker,taken_by_receiver,returned_to_locker,returned_to_receiver,cancelled")
                .Annotation("Npgsql:Enum:participant_type", "individual,legal_entity")
                .Annotation("Npgsql:Enum:payment_status", "paid,pending")
                .Annotation("Npgsql:Enum:postamat_state", "available,unavailable,under_maintenance")
                .Annotation("Npgsql:Enum:postamat_status", "vacant,busy")
                .Annotation("Npgsql:Enum:postamat_type", "mini,standard,compact,big")
                .Annotation("Npgsql:Enum:who_pays", "sender,receiver");

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    CityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    Code = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.CityId);
                });

            migrationBuilder.CreateTable(
                name: "Individual",
                columns: table => new
                {
                    IndividualId = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    MiddleName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    DocumentType = table.Column<int>(type: "integer", nullable: false),
                    Document = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    CountryCode = table.Column<string>(type: "character varying(5)", maxLength: 5, nullable: false),
                    PhoneNumber = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    Email = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Individual", x => x.IndividualId);
                });

            migrationBuilder.CreateTable(
                name: "LockerType",
                columns: table => new
                {
                    LockerTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    Heigh = table.Column<int>(type: "integer", nullable: false),
                    Width = table.Column<int>(type: "integer", nullable: false),
                    Depth = table.Column<int>(type: "integer", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LockerType", x => x.LockerTypeId);
                });

            migrationBuilder.CreateTable(
                name: "PostamatForm",
                columns: table => new
                {
                    PostamatFormId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PostamatType = table.Column<int>(type: "integer", nullable: false),
                    CellsCount = table.Column<int>(type: "integer", nullable: false),
                    PictureUrl = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostamatForm", x => x.PostamatFormId);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Key = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "LegalEntity",
                columns: table => new
                {
                    LegalEntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ShortName = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    FullName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    LegalAddressLine1 = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    LegalAddressLine2 = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    LegalAddressCityId = table.Column<Guid>(type: "uuid", nullable: true),
                    Email = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    PhoneNumber1 = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    PhoneNumber2 = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalEntity", x => x.LegalEntityId);
                    table.ForeignKey(
                        name: "FK_LegalEntity_City_LegalAddressCityId",
                        column: x => x.LegalAddressCityId,
                        principalTable: "City",
                        principalColumn: "CityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LockerLocation",
                columns: table => new
                {
                    LockerLocationId = table.Column<Guid>(type: "uuid", nullable: false),
                    Left = table.Column<int>(type: "integer", nullable: false),
                    Top = table.Column<int>(type: "integer", nullable: false),
                    LockerTypeId = table.Column<Guid>(type: "uuid", nullable: true),
                    PostamatFormId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LockerLocation", x => x.LockerLocationId);
                    table.ForeignKey(
                        name: "FK_LockerLocation_LockerType_LockerTypeId",
                        column: x => x.LockerTypeId,
                        principalTable: "LockerType",
                        principalColumn: "LockerTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LockerLocation_PostamatForm_PostamatFormId",
                        column: x => x.PostamatFormId,
                        principalTable: "PostamatForm",
                        principalColumn: "PostamatFormId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Postamat",
                columns: table => new
                {
                    PostamatId = table.Column<Guid>(type: "uuid", nullable: false),
                    AddressLine1 = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    AddressLine2 = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    HowToFindDescription = table.Column<string>(type: "text", nullable: true),
                    CityId = table.Column<Guid>(type: "uuid", nullable: true),
                    WithScreen = table.Column<bool>(type: "boolean", nullable: false),
                    PostamatState = table.Column<int>(type: "integer", nullable: false),
                    PostamatStatus = table.Column<int>(type: "integer", nullable: false),
                    UniqueCode = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    Latitude = table.Column<decimal>(type: "numeric", nullable: true),
                    Longitude = table.Column<decimal>(type: "numeric", nullable: true),
                    MapZoom = table.Column<decimal>(type: "numeric", nullable: true),
                    BarcodeString = table.Column<string>(type: "text", nullable: true),
                    Barcode = table.Column<byte[]>(type: "bytea", nullable: true),
                    PostamatFormId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Postamat", x => x.PostamatId);
                    table.ForeignKey(
                        name: "FK_Postamat_City_CityId",
                        column: x => x.CityId,
                        principalTable: "City",
                        principalColumn: "CityId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Postamat_PostamatForm_PostamatFormId",
                        column: x => x.PostamatFormId,
                        principalTable: "PostamatForm",
                        principalColumn: "PostamatFormId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Courier",
                columns: table => new
                {
                    CourierId = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    LastName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    MiddleName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    Email = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    LegalEntityId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courier", x => x.CourierId);
                    table.ForeignKey(
                        name: "FK_Courier_LegalEntity_LegalEntityId",
                        column: x => x.LegalEntityId,
                        principalTable: "LegalEntity",
                        principalColumn: "LegalEntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Participant",
                columns: table => new
                {
                    ParticipantId = table.Column<Guid>(type: "uuid", nullable: false),
                    ParticipantType = table.Column<int>(type: "integer", nullable: false),
                    LegalEntityId = table.Column<Guid>(type: "uuid", nullable: true),
                    IndividualId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participant", x => x.ParticipantId);
                    table.ForeignKey(
                        name: "FK_Participant_Individual_IndividualId",
                        column: x => x.IndividualId,
                        principalTable: "Individual",
                        principalColumn: "IndividualId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Participant_LegalEntity_LegalEntityId",
                        column: x => x.LegalEntityId,
                        principalTable: "LegalEntity",
                        principalColumn: "LegalEntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HowToFindPicture",
                columns: table => new
                {
                    PictureId = table.Column<Guid>(type: "uuid", nullable: false),
                    Url = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    PostamatId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HowToFindPicture", x => x.PictureId);
                    table.ForeignKey(
                        name: "FK_HowToFindPicture_Postamat_PostamatId",
                        column: x => x.PostamatId,
                        principalTable: "Postamat",
                        principalColumn: "PostamatId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Locker",
                columns: table => new
                {
                    LockerId = table.Column<Guid>(type: "uuid", nullable: false),
                    LockerLocationId = table.Column<Guid>(type: "uuid", nullable: false),
                    LockerState = table.Column<int>(type: "integer", nullable: false),
                    LockerStatus = table.Column<int>(type: "integer", nullable: false),
                    PostamatId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locker", x => x.LockerId);
                    table.ForeignKey(
                        name: "FK_Locker_LockerLocation_LockerLocationId",
                        column: x => x.LockerLocationId,
                        principalTable: "LockerLocation",
                        principalColumn: "LockerLocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Locker_Postamat_PostamatId",
                        column: x => x.PostamatId,
                        principalTable: "Postamat",
                        principalColumn: "PostamatId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Delivery",
                columns: table => new
                {
                    DeliveryId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlanDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    FactStartTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    FactEndTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    CourierId = table.Column<Guid>(type: "uuid", nullable: true),
                    DeliveryState = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Delivery", x => x.DeliveryId);
                    table.ForeignKey(
                        name: "FK_Delivery_Courier_CourierId",
                        column: x => x.CourierId,
                        principalTable: "Courier",
                        principalColumn: "CourierId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Parcel",
                columns: table => new
                {
                    ParcelId = table.Column<Guid>(type: "uuid", nullable: false),
                    Paid = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    ReceiverId = table.Column<Guid>(type: "uuid", nullable: true),
                    SenderId = table.Column<Guid>(type: "uuid", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Weight = table.Column<decimal>(type: "numeric", nullable: true),
                    Heigh = table.Column<decimal>(type: "numeric", nullable: true),
                    Width = table.Column<decimal>(type: "numeric", nullable: true),
                    Depth = table.Column<decimal>(type: "numeric", nullable: true),
                    PostamatId = table.Column<Guid>(type: "uuid", nullable: false),
                    LockerTypeId = table.Column<Guid>(type: "uuid", nullable: true),
                    DeliveryPaymentStatus = table.Column<int>(type: "integer", nullable: false),
                    DeliveryCost = table.Column<decimal>(type: "numeric", nullable: true),
                    ParcelState = table.Column<int>(type: "integer", nullable: true),
                    LockerId = table.Column<Guid>(type: "uuid", nullable: true),
                    MaxTakeOutDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    WhoPays = table.Column<int>(type: "integer", nullable: false),
                    ParcelCancellationReason = table.Column<int>(type: "integer", nullable: true),
                    BarcodeString = table.Column<string>(type: "text", nullable: true),
                    Barcode = table.Column<byte[]>(type: "bytea", nullable: true),
                    DeliveryId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parcel", x => x.ParcelId);
                    table.ForeignKey(
                        name: "FK_Parcel_Delivery_DeliveryId",
                        column: x => x.DeliveryId,
                        principalTable: "Delivery",
                        principalColumn: "DeliveryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Parcel_Locker_LockerId",
                        column: x => x.LockerId,
                        principalTable: "Locker",
                        principalColumn: "LockerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Parcel_LockerType_LockerTypeId",
                        column: x => x.LockerTypeId,
                        principalTable: "LockerType",
                        principalColumn: "LockerTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Parcel_Participant_ReceiverId",
                        column: x => x.ReceiverId,
                        principalTable: "Participant",
                        principalColumn: "ParticipantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Parcel_Participant_SenderId",
                        column: x => x.SenderId,
                        principalTable: "Participant",
                        principalColumn: "ParticipantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Parcel_Postamat_PostamatId",
                        column: x => x.PostamatId,
                        principalTable: "Postamat",
                        principalColumn: "PostamatId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ParcelStateHistory",
                columns: table => new
                {
                    ParcelStateHistoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    ParcelId = table.Column<Guid>(type: "uuid", nullable: false),
                    State = table.Column<int>(type: "integer", nullable: false),
                    Time = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParcelStateHistory", x => x.ParcelStateHistoryId);
                    table.ForeignKey(
                        name: "FK_ParcelStateHistory_Parcel_ParcelId",
                        column: x => x.ParcelId,
                        principalTable: "Parcel",
                        principalColumn: "ParcelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Courier_LegalEntityId",
                table: "Courier",
                column: "LegalEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_CourierId",
                table: "Delivery",
                column: "CourierId");

            migrationBuilder.CreateIndex(
                name: "IX_HowToFindPicture_PostamatId",
                table: "HowToFindPicture",
                column: "PostamatId");

            migrationBuilder.CreateIndex(
                name: "IX_LegalEntity_LegalAddressCityId",
                table: "LegalEntity",
                column: "LegalAddressCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Locker_LockerLocationId",
                table: "Locker",
                column: "LockerLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Locker_PostamatId",
                table: "Locker",
                column: "PostamatId");

            migrationBuilder.CreateIndex(
                name: "IX_LockerLocation_LockerTypeId",
                table: "LockerLocation",
                column: "LockerTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_LockerLocation_PostamatFormId",
                table: "LockerLocation",
                column: "PostamatFormId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_DeliveryId",
                table: "Parcel",
                column: "DeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_LockerId",
                table: "Parcel",
                column: "LockerId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_LockerTypeId",
                table: "Parcel",
                column: "LockerTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_PostamatId",
                table: "Parcel",
                column: "PostamatId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_ReceiverId",
                table: "Parcel",
                column: "ReceiverId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_SenderId",
                table: "Parcel",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_State",
                table: "Parcel",
                column: "ParcelState");

            migrationBuilder.CreateIndex(
                name: "IX_ParcelStateHistory_ParcelId",
                table: "ParcelStateHistory",
                column: "ParcelId");

            migrationBuilder.CreateIndex(
                name: "IX_Participant_IndividualId",
                table: "Participant",
                column: "IndividualId");

            migrationBuilder.CreateIndex(
                name: "IX_Participant_LegalEntityId",
                table: "Participant",
                column: "LegalEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Postamat_CityId",
                table: "Postamat",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Postamat_PostamatFormId",
                table: "Postamat",
                column: "PostamatFormId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HowToFindPicture");

            migrationBuilder.DropTable(
                name: "ParcelStateHistory");

            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.DropTable(
                name: "Parcel");

            migrationBuilder.DropTable(
                name: "Delivery");

            migrationBuilder.DropTable(
                name: "Locker");

            migrationBuilder.DropTable(
                name: "Participant");

            migrationBuilder.DropTable(
                name: "Courier");

            migrationBuilder.DropTable(
                name: "LockerLocation");

            migrationBuilder.DropTable(
                name: "Postamat");

            migrationBuilder.DropTable(
                name: "Individual");

            migrationBuilder.DropTable(
                name: "LegalEntity");

            migrationBuilder.DropTable(
                name: "LockerType");

            migrationBuilder.DropTable(
                name: "PostamatForm");

            migrationBuilder.DropTable(
                name: "City");
        }
    }
}
