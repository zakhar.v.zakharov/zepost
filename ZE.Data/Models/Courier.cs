﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table(nameof(Courier))]
    public class Courier
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CourierId { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(50)]

        public string MiddleName { get; set; }
        [StringLength(20)]

        public string PhoneNumber { get; set; }
        [StringLength(50)]
        public string Email { get; set; }

        [ForeignKey(nameof(LegalEntity))]
        public Guid? LegalEntityId { get; set; }
        public LegalEntity LegalEntity { get; set; }
    }
}
