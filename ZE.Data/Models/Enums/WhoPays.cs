﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum WhoPays
    {
        Sender = 1,
        Receiver = 2
    }
}
