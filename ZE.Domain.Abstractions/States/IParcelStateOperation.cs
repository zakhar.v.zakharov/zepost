﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data.Models;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.States
{
    public interface IParcelStateOperation
    {
        ParcelState State { get; }
        Task PerformAsync(Parcel parcel);
    }

}
