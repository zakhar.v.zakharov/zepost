﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;
using ZE.Domain.Services;
using ZE.Postamat.Seeds.Extensions;
using ZE.Postamat.Seeds.ForTesting;

namespace ZE.Postamat.Seeds
{
    class Program
    {
        static async Task Main(string[] args)
        {            
            using IHost host = CreateHostBuilder(args).Build();
            await DoAsync(host.Services);
            await host.RunAsync();
        }

        static async Task DoAsync(IServiceProvider services)
        {
            using IServiceScope serviceScope = services.CreateScope();
            IServiceProvider provider = serviceScope.ServiceProvider;

            await SeedManager.Create(provider.GetService<DataContext>(), typeof(SettingsSeed),
                                                                         typeof(CitySeed), 
                                                                         typeof(ParticipantSeed), 
                                                                         typeof(LockerTypesSeed), 
                                                                         typeof(PostamatFormSeed),
                                                                         typeof(PostamatSeed))
                .RunAsync();
        }

        static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).ConfigureServices((_, services) => {
                services
                    .AddLogging(configure => configure.AddConsole())
                    .ConfigureDatabase("Host=localhost;Database=ZE_postamats;Username=postgres;Password=Password12!")
                    .ConfigureServices()
                    ;
            });
        }
    }
}
