﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Postamat.Seeds.Extensions;

namespace ZE.Postamat.Seeds
{
    public abstract class BaseSeed : ISeed
    {
        public BaseSeed(DataContext context)
        {
            Context = context;
        }

        protected DataContext Context { get; }

        public abstract Task ProcessSeedAsync();

        protected async Task AddRecordUniqueBy<T>(T obj, Expression<Func<T, object>> expression) where T:class
        {            
            if (!Context.Set<T>().Any(expression.CompareByValues(obj)))
            {
                await Context.Set<T>().AddAsync(obj);                
            }
        }

        protected async Task<T> AddOrGetRecordUniqueBy<T>(T obj, Expression<Func<T, object>> expression) where T : class
        {
            var record = Context.Set<T>().FirstOrDefault(expression.CompareByValues(obj));
            if (record == null)
            {
                return (await Context.Set<T>().AddAsync(obj)).Entity;
            }
            return record;
        }
    }
}
