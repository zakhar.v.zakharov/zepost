import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { I18nModule } from '@app/i18n';
import { AuthModule } from '@app/auth';
import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import {AppSidebarComponent} from '../components/app-sidebar/app-sidebar.component';
import { NgxBootstrapIconsModule } from 'ngx-bootstrap-icons';
import { alarm, alarmFill, alignBottom, truck, boxSeam, mailbox, personFill } from 'ngx-bootstrap-icons';
const icons = {
  alarm,
  alarmFill,
  alignBottom,
  truck,
  boxSeam,
  mailbox,
  personFill
};


@NgModule({
  imports: [CommonModule, TranslateModule, NgbModule, AuthModule, I18nModule, RouterModule, NgxBootstrapIconsModule.pick(icons)],
  declarations: [HeaderComponent, ShellComponent, AppSidebarComponent],
})
export class ShellModule {}
