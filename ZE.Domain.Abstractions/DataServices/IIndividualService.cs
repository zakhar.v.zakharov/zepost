﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data.Models;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Abstractions.Services
{
    interface IIndividualService
    {
        Task<IEnumerable<IndividualListModel>> GetIndividualsAsync(int pageNo, int N, bool asc);
        Task<IEnumerable<IndividualListModel>>SearchIndividualsAsync(string statement);

        IQueryable<IndividualListModel> FindIndividuals();

        Task<Individual> CreateIndividualAsync(Individual model);

        Task<Individual> UpdateIndividualAsync(Individual model);

        Task DeleteIndividualAsync(Guid individualId);
    }
}
