﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ZE.Domain.Abstractions.TechnicalServices
{
    public interface IBarcodeGenerationService
    {
        byte[] GenerateBarcode(string info);
    }
}
