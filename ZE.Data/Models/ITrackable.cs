﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models
{
    public interface ITrackable
    {
        public User CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public DateTimeOffset Created { get; set; }

        public User UpdatedBy { get; set; }
        public Guid UpdatedById { get; set; }
        public DateTimeOffset Updated { get; set; }
    }
}
