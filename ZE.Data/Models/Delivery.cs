﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(Delivery))]
    public class Delivery : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DeliveryId { get; set; }

        public DateTimeOffset PlanDate { get; set; }

        public DateTimeOffset? FactStartTime { get; set; }

        public DateTimeOffset? FactEndTime { get; set; }
        
        [InverseProperty(nameof(Delivery))]
        public ICollection<Parcel> Parcels { get; set; }

        public Guid? CourierId { get; set; }
        public Courier Courier { get; set; }

        public DeliveryState DeliveryState { get; set; }

        public User CreatedBy { get; set; }
        [ForeignKey(nameof(CreatedBy))]
        public Guid CreatedById { get; set; }
        public DateTimeOffset Created { get; set; }
        public User UpdatedBy { get; set; }
        [ForeignKey(nameof(UpdatedBy))]
        public Guid UpdatedById { get; set; }
        public DateTimeOffset Updated { get; set; }

    }
}
