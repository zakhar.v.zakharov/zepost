﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(ParcelStateHistory))]
    public class ParcelStateHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ParcelStateHistoryId { get; set; }
        [InverseProperty("ParcelStateHistories")]
        public Parcel Parcel { get; set; }
        [ForeignKey(nameof(Parcel))]
        public Guid ParcelId { get; set; }
        public ParcelState State { get; set; }

        public DateTimeOffset Time { get; set; }
    }
}
