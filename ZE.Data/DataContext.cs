﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data.Models;
using ZE.Data.Models.Enums;

namespace ZE.Data
{
    public class DataContext : DbContext
    {        
        public DataContext() : base() 
        {         
        }    

        public DataContext(DbContextOptions options) : base(options) { }

        public DbSet<City> Cities { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Courier> Couriers { get; set; }
        public DbSet<HowToFindPicture> Pictures { get; set; }
        public DbSet<Individual> Individuals { get; set; }
        public DbSet<LegalEntity> LegalEntities { get; set; }
        public DbSet<Locker> Lockers { get; set; }
        public DbSet<LockerType> LockerTypes { get; set; }
        public DbSet<Parcel> Parcels { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Postamat> Postamats { get; set; }
        public DbSet<PostamatForm> PostamatForms { get; set; }

        public DbSet<Setting> Settings { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }

        public DbSet<ParcelStateHistory> ParcelStateHistories { get; set; }

        public DbSet<LockerLocation> LockerLocations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseNpgsql(o => o.EnableRetryOnFailure());
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                   .HasPostgresEnum<DocumentType>()
                   .HasPostgresEnum<LockerState>()
                   .HasPostgresEnum<LockerStatus>()
                   .HasPostgresEnum<ParcelState>()
                   .HasPostgresEnum<ParticipantType>()
                   .HasPostgresEnum<PaymentStatus>()
                   .HasPostgresEnum<PostamatState>()
                   .HasPostgresEnum<PostamatStatus>()
                   .HasPostgresEnum<WhoPays>()
                   .HasPostgresEnum<DeliveryState>()
                   .HasPostgresEnum<ParcelCancellationReason>()
                   .HasPostgresEnum<PostamatType>();

            builder.Entity<Parcel>()
                .HasIndex(p => p.ParcelState)
                .HasDatabaseName("IX_Parcel_State");
            builder.Entity<Participant>().HasMany<Parcel>(c => c.ParcelsIReceived).WithOne(c => c.Receiver).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Participant>().HasMany<Parcel>(c => c.ParcelsISent).WithOne(c => c.Sender).OnDelete(DeleteBehavior.Cascade);
        }


      
    }
}
