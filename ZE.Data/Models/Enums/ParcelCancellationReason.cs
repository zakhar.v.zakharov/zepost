﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum ParcelCancellationReason
    {
        NotPaid = 0,
        SenderRefuse = 1,
        ReceiverRefuse = 2
    }
}
