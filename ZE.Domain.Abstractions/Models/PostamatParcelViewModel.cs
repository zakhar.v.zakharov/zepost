﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Domain.Abstractions.Models
{
    public class PostamatParcelViewModel
    {
        public Guid PostamatId { get; set; }
        public string City { get; set; }
        public string Address { get; set; }

        public string Code { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
