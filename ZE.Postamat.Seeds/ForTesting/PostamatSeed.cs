﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;

namespace ZE.Postamat.Seeds.ForTesting
{
    [SeedOrder(5)]
    public class PostamatSeed : BaseSeed
    {
        public PostamatSeed(DataContext context) : base(context) { }
        public override async Task ProcessSeedAsync()
        {
            var postamatForm = Context.PostamatForms.FirstOrDefault(f => f.Name == "For Testing");
            var city = Context.Cities.FirstOrDefault(c => c.Code == "MSK");
            var postamat = new ZE.Data.Models.Postamat
            { 
                City = city,
                HowToFindDescription = "Супермаркет пятерочка",
                AddressLine1 = "Подмосковный бульвар, д.1",
                Latitude = 55.212332M,
                Longitude = 37.213123M,
                MapZoom = 10,
                PostamatState = Data.Models.Enums.PostamatState.Available,
                PostamatStatus = Data.Models.Enums.PostamatStatus.Vacant,
                WithScreen = false,
                UniqueCode = "MSKP001",
                PostamatForm = postamatForm                
            };
            postamat = await AddOrGetRecordUniqueBy(postamat, p => p.UniqueCode);

            var locations = Context.LockerLocations.Where(p => p.PostamatFormId == postamatForm.PostamatFormId);

            foreach(var location in locations)
            {
                var locker = new Locker
                {
                    LockerLocationId = location.LockerLocationId,
                    LockerState = Data.Models.Enums.LockerState.Closed,
                    LockerStatus = Data.Models.Enums.LockerStatus.Vacant,
                    Postamat = postamat
                };

                await AddRecordUniqueBy(locker, l => new { l.LockerLocationId });
            }

            await Context.SaveChangesAsync();
        }
    }
}
