﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.Models;
using ZE.Domain.Abstractions.Services;

namespace ZE.Logistics.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelController : ControllerBase
    {
        private readonly IParcelService _parcelService;
        public ParcelController(IParcelService parcelService)
        {
            _parcelService = parcelService;
        }

        [HttpGet]
        public async Task<IActionResult> GetParcels()
        {
            var parcels = await _parcelService.GetParcelsAsync();
            return Ok(parcels);
        }

        [HttpPost]
        public async Task<IActionResult> CreateParcel(ParcelComposeModel model)
        {
            var parcel = await _parcelService.CreateParcelAsync(model);
            return Ok(parcel);
        }
    }
}
