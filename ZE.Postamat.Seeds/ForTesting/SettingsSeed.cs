﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;

namespace ZE.Postamat.Seeds.ForTesting
{
    [SeedOrder(0)]
    public class SettingsSeed : BaseSeed
    {
        public SettingsSeed(DataContext context) : base(context) { }
        public override async Task ProcessSeedAsync()
        {
            await AddRecordUniqueBy(new Setting { Key = "BarcodeEncryptionKey", Value = @"""UjXn2r5u8x/A?D(G+KbPdSgVkYp3s6v9""" }, s => s.Key);
            await Context.SaveChangesAsync();
        }
    }
}
