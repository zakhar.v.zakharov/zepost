﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Abstractions.DataServices
{
    public interface IDeliveryService
    {
        Task<DeliveryEditModel> CreateDeliveryAsync(DeliveryEditModel model);
        Task<DeliveryEditModel> UpdateDeliveryAsync(DeliveryEditModel model);
        Task<DeliveryViewModel> AddParcelToDeliveryAsync(Guid parcelId, Guid deliveryId);
        Task<DeliveryViewModel> GetDeliveryAsync(Guid deliveryId);

        
    }
}
