﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum PaymentStatus
    {
        Paid = 1,
        Pending = 0        
    }
}
