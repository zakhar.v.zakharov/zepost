﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(Locker))]
    public class Locker
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid? LockerId { get; set; }

        [ForeignKey(nameof(LockerLocation))]
        public Guid LockerLocationId { get; set; }
        public LockerLocation LockerLocation { get; set; }

        public LockerState LockerState { get; set; }

        public LockerStatus LockerStatus { get; set; }

        [ForeignKey(nameof(Postamat))]
        public Guid PostamatId { get; set; }

        [InverseProperty("Lockers")]
        public Postamat Postamat { get; set; }
        
    }
}
