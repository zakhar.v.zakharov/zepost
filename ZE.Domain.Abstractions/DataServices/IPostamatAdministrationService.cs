﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Abstractions.Services
{
    public interface IPostamatAdministrationService
    {
        Task<IEnumerable<PostamatListModel>> GetPostamatsAsync();

        Task<PostamatEditModel> UpdatePostamatAsync(PostamatEditModel model);

        Task<PostamatEditModel> CreatePostamatAsync(PostamatEditModel model);

        Task DeletePostamatAsync(Guid postamatId);
        
    }
}
