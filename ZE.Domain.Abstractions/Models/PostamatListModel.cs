﻿using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.Models
{
    public class PostamatListModel
    {
        public Guid PostamatId { get; set; }
        
        public string City { get; set; }        
        public bool WithScreen { get; set; }

        public PostamatType PostamatType { get; set; }

        public PostamatState PostamatState { get; set; }
        public PostamatStatus PostamatStatus { get; set; }
        
        public string UniqueCode { get; set; }
    }
}
