﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum DocumentType
    {
        Passport,
        DrivingLicense
    }
}
