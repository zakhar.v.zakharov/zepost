﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Abstractions.Services
{
    public interface IDictionariesService
    {
        Task<IEnumerable<CityListModel>> GetCitiesAsync();

        Task<IEnumerable<CityListModel>> GetCitiesByNameAsync();

        Task<IEnumerable<PostamatFormListModel>> GetPostamatFormsAsync();
    }
}
