﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table(nameof(LockerLocation))]
    public class LockerLocation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LockerLocationId { get; set; }

        public int Left { get; set; }
        public int Top { get; set; }

        public LockerType LockerType { get; set; }

        [ForeignKey(nameof(PostamatForm))]
        public Guid PostamatFormId { get; set; }

        [InverseProperty("LockerLocations")]
        public PostamatForm PostamatForm { get; set; }
    }
}
