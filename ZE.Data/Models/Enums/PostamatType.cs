﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum PostamatType
    {                
        Mini = 1,
        Standard = 2,
        Compact = 3,
        Big = 4
    }
}
