﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ZE.Domain.Abstractions.DataServices
{
    public interface ISettingsService
    {
        Task<T> GetSettingAsync<T>(string key);
    }
}
