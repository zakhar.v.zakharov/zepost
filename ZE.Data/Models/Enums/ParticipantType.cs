﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum ParticipantType
    {
        Individual = 1,
        LegalEntity = 2
    }
}
