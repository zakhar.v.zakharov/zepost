﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ZE.Domain.Abstractions.TechnicalServices
{
    public interface IEncryptionService
    {
        Task<string> EncryptAsync<T>(T obj);

        Task<T> DecryptAsync<T>(string cipherText);
    }
}
