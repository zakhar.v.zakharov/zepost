﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Abstractions.Services
{
    public interface IParcelService
    {
        Task<ParcelViewModel> GetParcelAsync(Guid parcelId);
        Task<Guid> CreateParcelAsync(ParcelComposeModel model);

        Task<ParcelComposeModel> UpdateParcelAsync(ParcelComposeModel model);

        Task<IEnumerable<ParcelListModel>> GetParcelsAsync();
    }
}
