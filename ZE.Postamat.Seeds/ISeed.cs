﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ZE.Postamat.Seeds
{
    public interface ISeed
    {
        Task ProcessSeedAsync();        
    }
}
