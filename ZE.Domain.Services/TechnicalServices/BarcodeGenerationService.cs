﻿using NetBarcode;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.TechnicalServices;

namespace ZE.Domain.Services.TechnicalServices
{
    public class BarcodeGenerationService : IBarcodeGenerationService
    {
        public byte[] GenerateBarcode(string info)
        {
            var barcode = new Barcode(info);
            var array =  barcode.GetByteArray(ImageFormat.Png);
            return array;
        }
    }
}
