﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models;
using ZE.Data.Models.Enums;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Services.MapperProfiles
{
    public class ModelsMapperProfile : Profile
    {
        public ModelsMapperProfile()
        {
            CreateMap<ParcelComposeModel, Parcel>()
                .ForMember(d => d.Description, s => s.MapFrom(p => p.Description))
                .ForMember(d => d.Heigh, s => s.MapFrom(p => p.Heigh))
                .ForMember(d => d.Width, s => s.MapFrom(p => p.Width))
                .ForMember(d => d.Depth, s => s.MapFrom(p => p.Depth))
                .ForMember(d => d.Weight, s => s.MapFrom(p => p.Weight))
                .ForMember(d => d.PostamatId, s => s.MapFrom(p => p.PostamatId))
                .ForMember(d => d.ReceiverId, s => s.MapFrom(p => p.ReceiverId))
                .ForMember(d => d.SenderId, s => s.MapFrom(p => p.SenderId))
                .ForMember(d => d.WhoPays, s => s.MapFrom(p => p.WhoPays))
                .ForAllOtherMembers(d => d.Ignore());

            /*
             return await _context.Parcels
                .Include(p => p.Sender)
                .Include(p => p.Receiver)
                .Include(p => p.Sender.Individual)
                .Include(p => p.Receiver.Individual)
                .Include(p => p.Sender.LegalEntity)
                .Include(p => p.Receiver.LegalEntity)
                .Include(p => p.Postamat)
                .Include(p => p.Postamat.City)
                .Include(p => p.ParcelStateHistories)
                .Select(p => _mapper.Map<ParcelListModel>(p)).ToListAsync();
             */
            CreateMap<Parcel, ParcelListModel>()
                .ForMember(d => d.ParcelId, s => s.MapFrom(p => p.ParcelId))
                .ForMember(d => d.States, s => s.MapFrom(p => p.ParcelStateHistories))
                .ForMember(d => d.ParcelState, s => s.MapFrom(p => p.ParcelState))                
                .ForMember(d => d.Postamat, s => s.MapFrom(p => p.Postamat))
                .ForMember(d => d.Sender, s => s.MapFrom(p => p.Sender.ParticipantType==ParticipantType.LegalEntity? p.Sender.LegalEntity.ShortName:p.Sender.Individual.FirstName + " " + p.Sender.Individual.LastName))
                .ForMember(d => d.Receiver, s => s.MapFrom(p => p.Receiver.ParticipantType==ParticipantType.LegalEntity? p.Receiver.LegalEntity.ShortName: p.Sender.Individual.FirstName + " " + p.Sender.Individual.LastName))
                .ForAllOtherMembers(d => d.Ignore());

            CreateMap<ParcelStateHistory, ParcelStateView>()
                .ForMember(d => d.State, s => s.MapFrom(p => p.State))
                .ForMember(d => d.Time, s => s.MapFrom(p => p.Time));                

            CreateMap<Postamat, PostamatParcelViewModel>()
                .ForMember(d => d.PostamatId, s => s.MapFrom(p => p.PostamatId))
                .ForMember(d => d.City, s => s.MapFrom(p => p.City.Name))
                .ForMember(d => d.Address, s => s.MapFrom(p => $"{p.AddressLine1}, {p.AddressLine2}"))
                .ForMember(d => d.Code, s => s.MapFrom(p => p.UniqueCode))
                .ForMember(d => d.Latitude, s => s.MapFrom(p => p.Latitude))
                .ForMember(d => d.Longitude, s => s.MapFrom(p => p.Longitude))                
                ;

        }
    }
}
