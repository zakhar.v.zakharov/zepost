﻿using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.Models
{
    public class DeliveryViewModel
    {
        public Guid? DeliveryId { get; set; }

        public Guid? CourierId { get; set; }

        public List<ParcelListModel> Parcels { get; set; }

        public DateTimeOffset PlannedDate { get; set; }

        public DateTimeOffset? StartDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public DeliveryState DeliveryState { get; set; }

        public CourierDeliveryModel Courier { get; set; }

        public List<PostamatRouteModel> Route { get; set; }
    }
}
