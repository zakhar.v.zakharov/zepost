﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum DeliveryState
    {
        Scheduled = 0,
        Pending = 1,
        Completed = 2,
        Cancelled = 100
    }
}
