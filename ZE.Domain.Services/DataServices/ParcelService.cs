﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;
using ZE.Data.Models.Enums;
using ZE.Domain.Abstractions.BusinessServices;
using ZE.Domain.Abstractions.Models;
using ZE.Domain.Abstractions.Services;
using ZE.Domain.Services.States;

namespace ZE.Domain.Services.DataServices
{
    public class ParcelService : IParcelService
    {
        private readonly DataContext _context;
        private readonly ILogger<ParcelService> _logger;
        private readonly IMapper _mapper;        
        private readonly ParcelStateProcessor _stateProcessor;
        public ParcelService(DataContext context, ParcelStateProcessor stateProcessor, ILogger<ParcelService> logger, IMapper mapper)
        {
            _context = context;            
            _mapper = mapper;
            _logger = logger;
            _stateProcessor = stateProcessor;
        }

        public async Task<Guid> CreateParcelAsync(ParcelComposeModel model)
        {
            var parcel = _mapper.Map<Parcel>(model);            
            await _stateProcessor(ParcelState.Sent).PerformAsync(parcel);
            var p = await _context.Parcels.AddAsync(parcel);
            await _context.SaveChangesAsync();            
            return p.Entity.ParcelId;
        }

        public Task<ParcelViewModel> GetParcelAsync(Guid parcelId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ParcelListModel>> GetParcelsAsync()
        {
            return await _context.Parcels.ProjectTo<ParcelListModel>(_mapper.ConfigurationProvider).ToListAsync();            
        }

        public Task<ParcelComposeModel> UpdateParcelAsync(ParcelComposeModel model)
        {
            throw new NotImplementedException();
        }
    }
}
