﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;

namespace ZE.Postamat.Seeds.ForTesting
{
    [SeedOrder(1)]
    public class CitySeed : BaseSeed
    {
        public CitySeed(DataContext context) : base(context) { }

        public override async Task ProcessSeedAsync()
        {
            await AddRecordUniqueBy(new City { Name = "Moscow", Code = "MSK" }, c => c.Code);
            await AddRecordUniqueBy(new City { Name = "Saint Petersburg", Code = "SPB" }, c => c.Code);
            await Context.SaveChangesAsync();
        }
    }
}
