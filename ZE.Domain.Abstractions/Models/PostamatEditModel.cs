﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.Models
{
    public class PostamatEditModel
    {
        public Guid PostamatId { get; set; }

        [MaxLength(100)]
        public string AddressLine1 { get; set; }
        [MaxLength(100)]
        public string AddressLine2 { get; set; }

        public string HowToFindDescription { get; set; }   
                
        public Guid CityId { get; set; }

        public bool WithScreen { get; set; }

        public PostamatType PostamatType { get; set; }

        public PostamatState PostamatState { get; set; }
        public PostamatStatus PostamatStatus { get; set; }
        
        public string UniqueCode { get; set; }
    }
}
