﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(Participant))]
    public class Participant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ParticipantId { get; set; }

        public ParticipantType ParticipantType { get; set; }

        [ForeignKey(nameof(LegalEntity))]
        public Guid? LegalEntityId { get; set; }
        public LegalEntity LegalEntity { get; set; }

        [ForeignKey(nameof(Individual))]
        public Guid? IndividualId { get; set; }
        public Individual Individual { get; set; }

        [InverseProperty("Sender")]
        public ICollection<Parcel> ParcelsISent { get; set; }

        [InverseProperty("Receiver")]
        public ICollection<Parcel> ParcelsIReceived { get; set; }
    }
}
