﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum LockerStatus
    {
        Vacant = 0,
        Busy = 1
    }
}
