﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table(nameof(LegalEntity))]
    public class LegalEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LegalEntityId { get; set; }

        [StringLength(20)]
        public string ShortName { get; set; }
        [StringLength(100)]
        public string FullName { get; set; }
        [StringLength(100)]
        public string LegalAddressLine1 { get; set; }
        [StringLength(100)]
        public string LegalAddressLine2 { get; set; }
        
        [ForeignKey(nameof(LegalEntity))]
        public Guid? LegalAddressCityId { get; set; }
        public City LegalAddressCity { get; set; }

        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(20)]
        public string PhoneNumber1 { get; set; }
        [StringLength(20)]
        public string PhoneNumber2 { get; set; }        

        public bool IsActive { get; set; }

    }
}
