﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;
using ZE.Data.Models.Enums;
using ZE.Domain.Abstractions.States;

namespace ZE.Domain.Services.States
{
    public class BaseParcelState : IParcelStateOperation
    {

        private readonly DataContext _context;
        private readonly ILogger _logger;
        public BaseParcelState(DataContext context, ILogger logger)
        {
            _context = context;
            _logger = logger;            
        }
        public virtual ParcelState State => ParcelState.Created;

        public virtual async Task PerformAsync(Parcel parcel)
        {
            var history = new ParcelStateHistory();
            history.Parcel = parcel;
            history.State = State;
            history.Time = DateTimeOffset.Now;
            parcel.ParcelState = State;
            await _context.ParcelStateHistories.AddAsync(history);            
        }
    }
}
