﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Data.Models;

namespace ZE.Postamat.Seeds.ForTesting
{
    [SeedOrder(3)]
    public class LockerTypesSeed : BaseSeed
    {
        public LockerTypesSeed(DataContext context) : base(context) { }

        public override async Task ProcessSeedAsync()
        {
            await AddRecordUniqueBy(new LockerType
            {
                Code = "S",
                Depth = 30,
                Heigh = 10,
                Width = 30
            }, t => t.Code);
            await AddRecordUniqueBy(new LockerType
            {
                Code = "M",
                Depth = 30,
                Heigh = 20,
                Width = 30
            }, t => t.Code);
            await AddRecordUniqueBy(new LockerType
            {
                Code = "L",
                Depth = 30,
                Heigh = 30,
                Width = 30
            }, t => t.Code);

            await Context.SaveChangesAsync();
        }
    }
}
