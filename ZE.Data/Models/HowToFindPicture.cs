﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table(nameof(HowToFindPicture))]
    public class HowToFindPicture
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PictureId { get; set; }
        [MaxLength(500)]
        public string Url { get; set; }
        public int Order { get; set; }

        [ForeignKey(nameof(Postamat))]
        public Guid? PostamatId { get; set; }

        public Postamat Postamat { get; set; }
    }
}
