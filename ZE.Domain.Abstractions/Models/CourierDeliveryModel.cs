﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Domain.Abstractions.Models
{
    public class CourierDeliveryModel
    {
        public Guid CourierId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
    }
}
