﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(Parcel))]
    public class Parcel : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ParcelId { get; set; }
      
        public DateTimeOffset? Paid { get; set; }        

        [ForeignKey(nameof(Receiver))]
        public Guid? ReceiverId { get; set; }

        [InverseProperty("ParcelsIReceived")]
        public Participant Receiver { get; set; }

        [ForeignKey(nameof(Sender))]
        public Guid? SenderId { get; set; }
        [InverseProperty("ParcelsISent")]
        public Participant Sender { get; set; }


        public string Description { get; set; }

        public decimal? Weight { get; set; }

        public decimal? Heigh { get; set; }
        public decimal? Width { get; set; }
        public decimal? Depth { get; set; }

        [ForeignKey(nameof(Postamat))]
        public Guid PostamatId { get; set; }
        public Postamat Postamat { get; set; }

        public LockerType LockerType { get; set; }

        public PaymentStatus DeliveryPaymentStatus { get; set; }

        public decimal? DeliveryCost { get; set; }

        public ParcelState? ParcelState { get; set; }

        [InverseProperty(nameof(Parcel))]
        public ICollection<ParcelStateHistory> ParcelStateHistories { get; set; }

        [ForeignKey(nameof(Locker))]
        public Guid? LockerId { get; set; }
        public Locker Locker { get; set; }

        public DateTimeOffset? MaxTakeOutDate { get; set; }

        public WhoPays WhoPays { get; set; }

        public ParcelCancellationReason? ParcelCancellationReason { get; set; }        

        public string BarcodeString { get; set; }

        public byte[] Barcode { get; set; }

        [ForeignKey(nameof(Delivery))]
        public Guid? DeliveryId { get; set; }

        [InverseProperty("Parcels")]
        public Delivery Delivery { get; set; }
        

        public User CreatedBy { get; set; }
        [ForeignKey(nameof(CreatedBy))]
        public Guid CreatedById { get; set; }
        public DateTimeOffset Created { get; set; }
        public User UpdatedBy { get; set; }
        [ForeignKey(nameof(UpdatedBy))]
        public Guid UpdatedById { get; set; }
        public DateTimeOffset Updated { get; set; }
    }
}
