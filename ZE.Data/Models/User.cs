﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table(nameof(User))]
    public class User
    {
        public Guid UserId { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(200)]
        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }
        [StringLength(20)]
        public string Phone { get; set; }
    }
}
