﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Domain.Abstractions.Models
{
    public class PostamatFormListModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }        
        public int CellsCount { get; set; }
        public string PictureUrl { get; set; }

    }
}
