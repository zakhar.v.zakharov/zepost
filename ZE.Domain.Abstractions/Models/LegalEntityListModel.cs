﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Domain.Abstractions.Models
{
    public class LegalEntityListModel
    {
        public Guid LegalEntityId { get; set; }
        
        public string ShortName { get; set; }
        public string FullName { get; set; }        
        public string City { get; set; }                
        public string PhoneNumbers { get; set; }

        public Guid ParticipantId { get; set; }
    }
}
