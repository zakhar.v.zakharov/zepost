﻿using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models.Enums;
using ZE.Domain.Abstractions.States;

namespace ZE.Domain.Services.States
{
    public delegate IParcelStateOperation ParcelStateProcessor(ParcelState state);
    
}
