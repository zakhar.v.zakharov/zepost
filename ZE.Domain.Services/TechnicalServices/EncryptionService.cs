﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ZE.Domain.Abstractions.DataServices;
using ZE.Domain.Abstractions.TechnicalServices;

namespace ZE.Domain.Services.TechnicalServices
{
    public class EncryptionService : IEncryptionService
    {
        private readonly ISettingsService _settingsService;
        private readonly ILogger<EncryptionService> _logger;
        private readonly static string _key = "BarcodeEncryptionKey";
        public EncryptionService(ISettingsService settingsService, ILogger<EncryptionService> logger)
        {
            _settingsService = settingsService;
            _logger = logger;
        }
        public async Task<T> DecryptAsync<T>(string cipherText)
        {            
            var key = await _settingsService.GetSettingAsync<string>(_key);
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader(cryptoStream))
                        {
                            return JsonConvert.DeserializeObject<T>(await streamReader.ReadToEndAsync());
                        }
                    }
                }
            }
        }

        public async Task<string> EncryptAsync<T>(T obj)
        {
            var plainText = JsonConvert.SerializeObject(obj);
            var key = await _settingsService.GetSettingAsync<string>(_key);
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (var memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                        {
                            await streamWriter.WriteAsync(plainText);
                        }
                        array = memoryStream.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);
        }
    }
}
