﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZE.Data.Migrations
{
    public partial class AddedUserAndTrackable : Migration
    {
        
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    LastName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Email = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    IsEmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    Phone = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });
            var rootId = new Guid("4a45bf18-3328-4022-9293-401817ba4a55");
            migrationBuilder.InsertData("User", new string[] { "UserId", "FirstName", "Email", "IsEmailConfirmed" }, new object[] { rootId, "Root", "root@zepost.cy", true });


            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "Postamat",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: DateTimeOffset.Now);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "Postamat",
                type: "uuid",
                nullable: false,
                defaultValue: rootId);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Updated",
                table: "Postamat",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: DateTimeOffset.Now);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedById",
                table: "Postamat",
                type: "uuid",
                nullable: false,
                defaultValue: rootId);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "Parcel",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: DateTimeOffset.Now);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "Parcel",
                type: "uuid",
                nullable: false,
                defaultValue: rootId);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Updated",
                table: "Parcel",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: DateTimeOffset.Now);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedById",
                table: "Parcel",
                type: "uuid",
                nullable: false,
                defaultValue: rootId);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "Delivery",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: DateTimeOffset.Now);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "Delivery",
                type: "uuid",
                nullable: false,
                defaultValue: rootId);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Updated",
                table: "Delivery",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: DateTimeOffset.Now);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedById",
                table: "Delivery",
                type: "uuid",
                nullable: false,
                defaultValue: rootId);

            migrationBuilder.CreateIndex(
                name: "IX_Postamat_CreatedById",
                table: "Postamat",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Postamat_UpdatedById",
                table: "Postamat",
                column: "UpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_CreatedById",
                table: "Parcel",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Parcel_UpdatedById",
                table: "Parcel",
                column: "UpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_CreatedById",
                table: "Delivery",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_UpdatedById",
                table: "Delivery",
                column: "UpdatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Delivery_User_CreatedById",
                table: "Delivery",
                column: "CreatedById",
                principalTable: "User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Delivery_User_UpdatedById",
                table: "Delivery",
                column: "UpdatedById",
                principalTable: "User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Parcel_User_CreatedById",
                table: "Parcel",
                column: "CreatedById",
                principalTable: "User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Parcel_User_UpdatedById",
                table: "Parcel",
                column: "UpdatedById",
                principalTable: "User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Postamat_User_CreatedById",
                table: "Postamat",
                column: "CreatedById",
                principalTable: "User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Postamat_User_UpdatedById",
                table: "Postamat",
                column: "UpdatedById",
                principalTable: "User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);            
            

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Delivery_User_CreatedById",
                table: "Delivery");

            migrationBuilder.DropForeignKey(
                name: "FK_Delivery_User_UpdatedById",
                table: "Delivery");

            migrationBuilder.DropForeignKey(
                name: "FK_Parcel_User_CreatedById",
                table: "Parcel");

            migrationBuilder.DropForeignKey(
                name: "FK_Parcel_User_UpdatedById",
                table: "Parcel");

            migrationBuilder.DropForeignKey(
                name: "FK_Postamat_User_CreatedById",
                table: "Postamat");

            migrationBuilder.DropForeignKey(
                name: "FK_Postamat_User_UpdatedById",
                table: "Postamat");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropIndex(
                name: "IX_Postamat_CreatedById",
                table: "Postamat");

            migrationBuilder.DropIndex(
                name: "IX_Postamat_UpdatedById",
                table: "Postamat");

            migrationBuilder.DropIndex(
                name: "IX_Parcel_CreatedById",
                table: "Parcel");

            migrationBuilder.DropIndex(
                name: "IX_Parcel_UpdatedById",
                table: "Parcel");

            migrationBuilder.DropIndex(
                name: "IX_Delivery_CreatedById",
                table: "Delivery");

            migrationBuilder.DropIndex(
                name: "IX_Delivery_UpdatedById",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Postamat");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Postamat");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Postamat");

            migrationBuilder.DropColumn(
                name: "UpdatedById",
                table: "Postamat");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Parcel");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Parcel");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Parcel");

            migrationBuilder.DropColumn(
                name: "UpdatedById",
                table: "Parcel");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "UpdatedById",
                table: "Delivery");
        }
    }
}
