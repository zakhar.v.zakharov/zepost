﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table(nameof(LockerType))]
    public class LockerType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LockerTypeId { get; set; }

        public int Heigh { get; set; }
        public int Width { get; set; }
        public int Depth { get; set; }

        public string Code { get; set; }             
    }
}
