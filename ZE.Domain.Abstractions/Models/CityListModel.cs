﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Domain.Abstractions.Models
{
    public class CityListModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
