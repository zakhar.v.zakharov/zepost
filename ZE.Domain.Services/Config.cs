﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using ZE.Domain.Abstractions.Services;
using ZE.Domain.Abstractions.States;
using ZE.Domain.Services.DataServices;
using ZE.Domain.Services.MapperProfiles;
using ZE.Domain.Services.States;
using AutoMapper;
using ZE.Domain.Abstractions.TechnicalServices;
using ZE.Domain.Services.TechnicalServices;
using ZE.Domain.Abstractions.DataServices;

namespace ZE.Domain.Services
{
    public static class Config
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services
                .AddTransient<IParcelService, ParcelService>()
                .AddTransient<SentParcelState>()                
                .AddTransient<IBarcodeGenerationService, BarcodeGenerationService>()
                .AddTransient<IEncryptionService, EncryptionService>()
                .AddTransient<ISettingsService, SettingsService>()
                .AddAutoMapper(typeof(ModelsMapperProfile));

            services.AddTransient<ParcelStateProcessor>(provider => state =>
            {
                switch (state)
                {
                    case Data.Models.Enums.ParcelState.Sent:
                        return provider.GetRequiredService<SentParcelState>();
                    default:
                        return provider.GetRequiredService<BaseParcelState>();
                }
            });
            return services;
        }
    }
}
