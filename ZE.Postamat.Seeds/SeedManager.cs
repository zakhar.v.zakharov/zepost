﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;

namespace ZE.Postamat.Seeds
{
    public class SeedManager
    {
        public static SeedManager Create(DataContext context, params Type[] types)
        {
            SortedList<int, ISeed> seeds = new SortedList<int, ISeed>();
            foreach (var type in types)
            {
                if (!type.GetInterfaces().Contains(typeof(ISeed)))
                    throw new Exception($"Type {type.Name} doesn't implement ISeed interface");
                var attribute = type.GetCustomAttributes(typeof(SeedOrderAttribute), false).FirstOrDefault();
                if (attribute != null)
                    seeds[(attribute as SeedOrderAttribute).Order] = (ISeed)Activator.CreateInstance(type, context);
            }
            return new SeedManager(seeds);
        }

        private readonly SortedList<int, ISeed> _seeds;
        private SeedManager(SortedList<int, ISeed> seeds)
        {
            _seeds = seeds;
        }

        public async Task RunAsync()
        {
            foreach(var seed in _seeds)
            {
                await seed.Value.ProcessSeedAsync();
            }
        }
       
    }
}
