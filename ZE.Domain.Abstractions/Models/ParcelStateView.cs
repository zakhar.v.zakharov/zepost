﻿using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.Models
{
    public class ParcelStateView
    {
        public DateTimeOffset Time { get; set; }
        public ParcelState State { get; set; }
    }
}
