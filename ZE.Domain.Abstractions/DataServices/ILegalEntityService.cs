﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data.Models;
using ZE.Domain.Abstractions.Models;

namespace ZE.Domain.Abstractions.Services
{
    public interface ILegalEntityService
    {
        Task<IEnumerable<LegalEntityListModel>> GetLegalEntitiesAsync();

        Task<IEnumerable<LegalEntityListModel>> SearchLegalEntitiesAsync(string statement);

        Task<LegalEntity> CreateLegalEntityAsync(LegalEntity model);

        Task<LegalEntity> UpdateLegalEntityAsync(LegalEntity model);

        Task DeleteLegalEntityAsync(Guid legalEntityId);

    }
}
