﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZE.Data.Models
{
    [Table("Settings")]
    public class Setting
    {
        [Key]
        public string Key { get; set; }
        [Column(TypeName = "jsonb")]
        public string Value { get; set; }
    }
}
