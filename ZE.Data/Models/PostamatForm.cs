﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(PostamatForm))]
    public class PostamatForm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PostamatFormId { get; set; }

        [Required]        
        public string Name { get; set; }
        public PostamatType PostamatType { get; set; }

        public int CellsCount { get; set; }

        [InverseProperty(nameof(PostamatForm))]
        public ICollection<LockerLocation> LockerLocations { get; set; }

        public string PictureUrl { get; set; }
        
        [InverseProperty(nameof(PostamatForm))]
        public ICollection<Postamat> Postamats { get; set; }
    }
}
