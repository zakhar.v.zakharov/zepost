﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum LockerState
    {
        Closed = 0,
        OpenedForDelivery = 1,
        OpenedForTakeOut = 2,
        OpenedForMaintenance = 100
    }
}
