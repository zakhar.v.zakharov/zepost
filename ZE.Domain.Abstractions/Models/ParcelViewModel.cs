﻿using System;
using System.Collections.Generic;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.Models
{
    public class ParcelViewModel
    {
        public Guid ParcelId { get; set; }      

        public Guid? SenderId { get; set; }
        public string SenderName { get; set; }
        public Guid? ReceiverId { get; set; }
        public string ReceiverName { get; set; }       

        public string Description { get; set; }

        public decimal Weight { get; set; }

        public decimal Heigh { get; set; }
        public decimal Width { get; set; }
        public decimal Depth { get; set; }

        public PaymentStatus DeliveryPaymentStatus { get; set; }

        public DateTimeOffset? Paid { get; set; }

        public decimal? DeliveryCost { get; set; }

        public WhoPays WhoPays { get; set; }

        public PostamatParcelViewModel Postamat { get; set; }

        public ParcelState ParcelState { get; set; }

        public Guid? CourierId { get; set; }
        public string CourierName { get; set; }

        public List<ParcelStateView> StateHistory { get; set; }
    }
}
