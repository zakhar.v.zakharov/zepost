﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ZE.Postamat.Seeds.Extensions
{
    public static class ReflectionExtensions
    {
        public static Expression<Func<T, bool>> CompareByValues<T>(this Expression<Func<T,object>> expression, T obj)
        {
            Expression<Func<T, bool>> result = null;
            var par = expression.Parameters[0];
            var body = expression.Body;
            if(body is NewExpression)
            {
                var exp = body as NewExpression;
                BinaryExpression t = null;
                foreach(var member in exp.Members)
                {
                    var prop = typeof(T).GetProperty(((PropertyInfo)member).Name);
                    var memberVal = prop.GetValue(obj);
                    if (memberVal == null || (memberVal is Guid && (Guid)memberVal == Guid.Empty) )
                    {
                        return Expression.Lambda<Func<T, bool>>(Expression.Constant(false), par);
                    }

                    var eqExp = Expression.Equal(Expression.Property(par, prop), Expression.Convert(Expression.Constant(memberVal), prop.PropertyType));
                    if (t == null)
                        t = eqExp;
                    else 
                    {
                        t = Expression.And(eqExp, t);
                    }
                }
                result = Expression.Lambda<Func<T, bool>>(t, par);
            }
            else if(body is MemberExpression)
            {
                var exp = body as MemberExpression;
                var prop = ((PropertyInfo)exp.Member);
                var memberVal = prop.GetValue(obj);
                if (memberVal == null || (memberVal is Guid && (Guid)memberVal == Guid.Empty))
                {
                    return Expression.Lambda<Func<T, bool>>(Expression.Constant(false), par);
                }
                var e =  Expression.Equal(Expression.Property(par, (PropertyInfo)exp.Member), Expression.Convert(Expression.Constant(memberVal), prop.PropertyType));
                result = Expression.Lambda<Func<T, bool>>(e, par);
            }            

            return result;
            
        }
    }
}
