﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Domain.Abstractions.Models
{
    public class IndividualListModel
    {
        public Guid IndividualId { get; set; }
        
        public string FullName { get; set; }        
                
        public string PhoneNumber { get; set; }

        public Guid ParticipantId { get; set; }

    }
}
