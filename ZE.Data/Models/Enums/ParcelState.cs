﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZE.Data.Models.Enums
{
    public enum ParcelState
    {
        Created = 0,
        Sent = 1,
        TakenToDelivery = 0,
        PendingDelivery = 2,
        DeliveredToLocker = 3,
        TakenByReceiver = 4,
        ReturnedToLocker = 5,
        ReturnedToReceiver = 6,
        Cancelled = 100
    }
}
