﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ZE.Data.Models.Enums;

namespace ZE.Data.Models
{
    [Table(nameof(Individual))]
    public class Individual
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid IndividualId { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

        [StringLength(50)]
        [Required]
        public string LastName { get; set; }
        [StringLength(50)]
        public string MiddleName { get; set; }
        
        public DocumentType DocumentType { get; set; }

        [StringLength(100)]
        public string Document { get; set; }

        [StringLength(5)]
        [Required]
        public string CountryCode { get; set; }

        [StringLength(20)]
        [Required]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

    }
}
