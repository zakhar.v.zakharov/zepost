﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZE.Data;
using ZE.Domain.Abstractions.DataServices;

namespace ZE.Domain.Services.DataServices
{
    public class SettingsService : ISettingsService
    {
        private readonly DataContext _context;
        public SettingsService(DataContext context) => _context = context;
        public async Task<T> GetSettingAsync<T>(string key)
        {
            var setting = await _context.Settings.FindAsync(key);
            return JsonConvert.DeserializeObject<T>(setting.Value);
        }
    }
}
