﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZE.Data.Models;
using ZE.Data.Models.Enums;

namespace ZE.Domain.Abstractions.BusinessServices
{
    public interface IParcelOperationsService
    {
        Task AssignBarcodeAsync(Parcel parcel);

        Task<Locker> AssignLockerAsync(Parcel parcel);

        Task<Delivery> AssignDeliveryAsync(Parcel parcel);

        Task<ParcelStateHistory> AssignStateAsync(Parcel parcel, ParcelState state);

        Task AssignMaxStorageDateAsync(Parcel parcel);
    }
}
